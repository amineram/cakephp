<!DOCTYPE html>
<html>

<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
	echo $this->fetch('meta');
	echo $this->Html->css('bootstrap.min');
	?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark p-2">
		<div class="container-fluid">
			<a class="navbar-brand" href="<?= $this->Html->url(['controller' => 'posts','action' => 'index']); ?>">Posts</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<!-- <a class="nav-link active" aria-current="page" href="">add</a> -->
					</li>
				</ul>
				<span class="navbar-text">
					username
					<i class="bi bi-person-circle"></i>
				</span>
			</div>
		</div>
	</nav>
	<div id="container" class="container">
		<?php
		echo $this->Session->flash();
		echo $this->fetch('content');
		?>
	</div>
	<?php
	echo $this->element('sql_dump');
	echo $this->Html->script('bootstrap.min');
	?>
</body>

</html>
