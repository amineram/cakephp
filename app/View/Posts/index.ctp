<a class="btn btn-primary mt-3" aria-current="page" href="<?= $this->Html->url(['controller' => 'posts', 'action' => 'add']); ?>"><i class="bi bi-plus-circle"></i> add new post</a>
<table class="table table-striped mt-3">
    <thead>
        <tr>
            <th scope="col">Title</th>
            <th scope="col">Slug</th>
            <th scope="col">Description</th>
            <th scope="col">Active</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($pages as $page) : ?>
           <tr>
               <td><?= $page['Post']['title'] ?></td>
               <td><?= $page['Post']['slug'] ?></td>
               <td><?= strlen($page['Post']['description']) > 50? substr($page['Post']['description'],0,50).' ...' : $page['Post']['description'] ?></td>
               <td><?= $page['Post']['active'] == 1 ? '<i class="bi bi-check"></i>' : '' ?></td>
               <td>
                   <a style="text-decoration:none;padding-left:1rem;color:black" href="<?= $this->Html->url(['controller' => 'posts','action' => 'show',$page['Post']['id']]); ?>">
                       <i class="bi bi-eye-fill"></i>
                    </a>
                   <a style="text-decoration:none;padding-left:1rem;color:black" href="<?= $this->Html->url(['controller' => 'posts','action' => 'edit',$page['Post']['id']]); ?>">
                       <i class="bi bi-pencil"></i>
                    </a>
                   <a style="text-decoration:none;padding-left:1rem;color:black" href="<?= $this->Html->url(['controller' => 'posts','action' => 'delete',$page['Post']['id']]); ?>">
                       <i class="bi bi-trash-fill"></i>
                    </a>
                </td>
               
            </tr> 
        <?php endforeach ?>
    </tbody>
</table>