<div class="form m-2">
    <?= $this->Form->create('Post') ?>

    <div class="mb-3">
        <label for="PostTitle" class="form-label">Title</label>
        <?= $this->Form->input('title', ['label' => false, 'required' => 'required', 'class' => "form-control"]); ?>
    </div>
    <div class="mb-3">
        <label for="PostDescription" class="form-label">Description</label>
        <?= $this->Form->input('description', ['label' => false, 'required' => 'required', 'class' => "form-control"]); ?>
    </div>
    <div class="mb-3">
        <label for="PostActive" class="form-label pr-1">Active</label>
        <?= $this->Form->checkbox('active', ['label' => false, 'class' => "form-check-input"]); ?>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    <?= $this->Form->end() ?>
</div>