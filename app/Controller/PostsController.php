<?php
class PostsController extends AppController{

    public function index(){
        $this->set('pages',$this->Post->find('all'));
    }

    public function show(){

    }

    public function add(){
        if($this->request->is('post')){
            
            if (!empty($this->request->data)) {
                $this->Post->create();
                $this->Post->save($this->request->data);
            }

        }
        
    }

    public function edit($id = null){
        $this->Post->id = $id;
        if (!$this->request->data) {
            $this->request->data = $this->Post->findById($id);
        }
        if($this->request->is('put')){
            
            if (!empty($this->request->data)) {
                $this->Post->create();
                $this->Post->id = $id;
                $this->Post->save($this->request->data);
            }

        }
        
    }

    public function delete(){

    }

}